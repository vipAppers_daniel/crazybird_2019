package com.example.crazybird_daniel1.Graphics;

import android.graphics.Paint;
import android.graphics.Typeface;

public class Cores {


    public static Paint getCorDoPassaro() {
        Paint vermelho = new Paint();
        vermelho.setColor(0x20FF0000);
        return vermelho;
    }

    public static Paint getCorCanoInferior() {
        Paint cinza = new Paint();
        cinza.setColor(0x10777777);
        return cinza;
    }

    public static Paint getCorCanoSuperior() {
        Paint verde = new Paint();
        verde.setColor(0xFF00FF00);
        return verde;
    }

    public static Paint getCorDoPontuacao() {
        Paint branco = new Paint();
        branco.setColor(0xFFFFFFFF);
        branco.setTextSize(120);
        branco.setTypeface(Typeface.DEFAULT_BOLD);
        branco.setShadowLayer(3,5,5, 0xFF000000);

        return branco;
    }

    public static Paint getCorDoGameOver() {
        Paint vermelhoGameOver = new Paint();
        vermelhoGameOver.setColor(0xFFFF0000);
        vermelhoGameOver.setTextSize(100);
        vermelhoGameOver.setTypeface(Typeface.DEFAULT_BOLD);
        vermelhoGameOver.setShadowLayer(3,5,5, 0xFF000000);

        return vermelhoGameOver;
    }
}
