package com.example.crazybird_daniel1.Engine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.example.crazybird_daniel1.Elements.Canos;
import com.example.crazybird_daniel1.Elements.GameOver;
import com.example.crazybird_daniel1.Elements.Passaro;
import com.example.crazybird_daniel1.Elements.Pontuacao;
import com.example.crazybird_daniel1.Graphics.Tela;
import com.example.crazybird_daniel1.MainActivity;
import com.example.crazybird_daniel1.R;

public class Game extends SurfaceView implements Runnable, View.OnTouchListener {

    private boolean isRunning = true;
    private final SurfaceHolder holder = getHolder();

    private Tela tela;
    private Passaro passaro;
    private Canos canos;
    private Pontuacao pontuacao;
    private Som som;


    private Bitmap background;

    public Game(Context context) {
        super(context);

        this.som = new Som(context);
        this.tela = new Tela(context);

        inicializaElementos(context);

        setOnTouchListener(this);
    }


    private void inicializaElementos(Context context) {

        this.passaro = new Passaro(tela, context, som);
        this.pontuacao = new Pontuacao();
        this.canos = new Canos(tela, pontuacao, context, som);

        Bitmap back = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        background = Bitmap.createScaledBitmap(back, tela.getLargura(), tela.getAltura(), false);
    }


    @Override
    public void run() {

        while (isRunning){
            if (!holder.getSurface().isValid()) continue;
            Canvas canvas = holder.lockCanvas();

            canvas.drawBitmap(background,0,0,null);

            canos.desenhaNo(canvas);
            passaro.desenhaNo(canvas);
            pontuacao.desenhaNo(canvas);

            if (new VerificadorDeColisao(passaro, canos).temColisao()) {
                new GameOver(tela).desenhaNo(canvas);
                this.isRunning = false;
                som.play(Som.COLISAO);
            }

            canos.move();
            passaro.cai();

            holder.unlockCanvasAndPost(canvas);
        }

    }


    public void pause() {
        this.isRunning = false;
    }

    public void inicia() {
        this.isRunning = true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        passaro.pula();
        return false;
    }
}