package com.example.crazybird_daniel1.Engine;

import com.example.crazybird_daniel1.Elements.Canos;
import com.example.crazybird_daniel1.Elements.Passaro;

class VerificadorDeColisao {

    private Passaro passaro;
    private Canos canos;


    public VerificadorDeColisao(Passaro passaro, Canos canos) {
        this.passaro = passaro;
        this.canos = canos;
    }

    public boolean temColisao() {
        return canos.temColisaoCom(passaro);
    }
}
