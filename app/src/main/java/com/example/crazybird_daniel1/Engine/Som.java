package com.example.crazybird_daniel1.Engine;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.example.crazybird_daniel1.R;

public class Som {

    private SoundPool soundPoll;

    public static int PULO;
    public static int COLISAO;
    public static int PONTOS;

    public Som(Context context){
        soundPoll = new SoundPool(3, AudioManager.STREAM_MUSIC,0);

        PONTOS = soundPoll.load(context, R.raw.pontos, 1);
        COLISAO = soundPoll.load(context, R.raw.colisao, 2);
        PULO = soundPoll.load(context, R.raw.pulo, 3);
    }


    public void play(int som) {
        soundPoll.play(som, 1,1,1,0,1);
    }

}
