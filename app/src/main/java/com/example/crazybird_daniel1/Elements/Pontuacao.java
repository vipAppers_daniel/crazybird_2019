package com.example.crazybird_daniel1.Elements;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.crazybird_daniel1.Graphics.Cores;

public class Pontuacao {

    private int pontos = 0;
    private static final Paint BRANCO = Cores.getCorDoPontuacao();

    public void aumenta() {
        pontos ++;
    }

    public void desenhaNo(Canvas canvas) {
        canvas.drawText(String.valueOf(pontos),80,150, BRANCO);
    }

    public int getPontos() {
        return pontos;
    }
}
