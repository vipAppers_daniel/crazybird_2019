package com.example.crazybird_daniel1.Elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.crazybird_daniel1.Graphics.Cores;
import com.example.crazybird_daniel1.Graphics.Tela;
import com.example.crazybird_daniel1.R;

public class Cano {

    private static final int LARGURA_DO_CANO = 175;

    private static final Paint CINZA = Cores.getCorCanoInferior();
    private static final Paint VERDE = Cores.getCorCanoSuperior();

    private int alturaDoCanoInferior;
    private int alturaDoCanoSuperior;

    private Bitmap canoSuperior;
    private Bitmap canoInferior;

    private Pontuacao pontuacao;

    private int TAMANHO_DO_CANO = 350;

    private int velocidade = 5;
    private int nivelDificuldade = 25;

    private int position;

    private Tela tela;

    private int canoUp = 250;

    private Bitmap imagemDoCanoSuperior;
    private Bitmap imagemDoCanoInferior;

    public Cano (Tela tela, int position, Pontuacao pontuacao, Context context) {

        this.pontuacao = pontuacao;

        this.position = position;
        this.tela = tela;

        this.alturaDoCanoInferior = tela.getAltura() - TAMANHO_DO_CANO - valorAleatorio();
        this.alturaDoCanoSuperior = TAMANHO_DO_CANO + valorAleatorio();

        imagemDoCanoSuperior = BitmapFactory.decodeResource(context.getResources(), R.drawable.cano_superior);
        imagemDoCanoInferior = BitmapFactory.decodeResource(context.getResources(), R.drawable.cano_inferior);

        canoSuperior = Bitmap.createScaledBitmap(imagemDoCanoSuperior, LARGURA_DO_CANO, alturaDoCanoSuperior, false);
        canoInferior = Bitmap.createScaledBitmap(imagemDoCanoInferior, LARGURA_DO_CANO, tela.getAltura() - alturaDoCanoInferior, false);
    }

    private int valorAleatorio() {
        return (int) (Math.random() * canoUp);
    }


    public void desenhaNo(Canvas canvas) {
        desenhaCanoInferiorNo(canvas);
        desenhaCanoSuperiorNo(canvas);
    }


    private void desenhaCanoSuperiorNo(Canvas canvas) {
        canvas.drawRect(position, 0,position + LARGURA_DO_CANO, alturaDoCanoSuperior, CINZA);
        canvas.drawBitmap(canoSuperior, position, 0,null);
    }


    private void desenhaCanoInferiorNo(Canvas canvas) {
        canvas.drawRect(position, alturaDoCanoInferior,position + LARGURA_DO_CANO, tela.getAltura(), CINZA);
        canvas.drawBitmap(canoInferior, position, alturaDoCanoInferior,null);

    }


    public void move() {
        position -= velocidade;

        int pontos = pontuacao.getPontos();

        if (pontos > nivelDificuldade) {
            velocidade += 1;
            nivelDificuldade += 25;
            canoUp += 50;
            TAMANHO_DO_CANO += 50;

            canoSuperior = Bitmap.createScaledBitmap(imagemDoCanoSuperior, LARGURA_DO_CANO, alturaDoCanoSuperior, false);
            canoInferior = Bitmap.createScaledBitmap(imagemDoCanoInferior, LARGURA_DO_CANO, tela.getAltura() - alturaDoCanoInferior, false);
        }
    }


    public boolean saiuDaTela() {
        return position + LARGURA_DO_CANO < 0;
    }

    public int getPosition() {
        return position;
    }

    public boolean temColisaoHorizontalCom(Passaro passaro) {
        return this.position - passaro.X < passaro.RAIO;
    }

    public boolean temColisaoVerticalCom(Passaro passaro) {
        return passaro.getAltura() - passaro.RAIO < this.alturaDoCanoSuperior
                || passaro.getAltura() + passaro.RAIO > this.alturaDoCanoInferior;
    }
}
