package com.example.crazybird_daniel1.Elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.crazybird_daniel1.Engine.Som;
import com.example.crazybird_daniel1.Graphics.Cores;
import com.example.crazybird_daniel1.Graphics.Tela;
import com.example.crazybird_daniel1.R;

public class Passaro {

    private static final Paint VERMELHO = Cores.getCorDoPassaro();

    int X = 100;
    int RAIO = 75;

    private Tela tela;
    private Som som;

    private int altura;
    private Bitmap passaro;

    public Passaro(Tela tela, Context context, Som som) {
        this.tela = tela;
        this.altura = tela.getAltura()/2;
        this.som = som;

        Bitmap imagemDoPassaro = BitmapFactory.decodeResource(context.getResources(), R.drawable.passaro);
        passaro = Bitmap.createScaledBitmap(imagemDoPassaro, RAIO * 2, RAIO * 2, false);
    }


    public void desenhaNo(Canvas canvas) {
        canvas.drawCircle(X, altura, RAIO, VERMELHO);
        canvas.drawBitmap(passaro, X - RAIO,altura - RAIO, null);
    }


    public void cai() {
        boolean chegouNoChao = altura + RAIO > tela.getAltura();

        if (!chegouNoChao) {
            this.altura += 5;
        }
    }


    public void pula() {
        if (altura > RAIO) {
            som.play(Som.PULO);
            this.altura -= 150;
        }
    }

    public int getAltura() {
        return this.altura;
    }
}
