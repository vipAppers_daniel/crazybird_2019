package com.example.crazybird_daniel1.Elements;

import android.content.Context;
import android.graphics.Canvas;

import com.example.crazybird_daniel1.Engine.Som;
import com.example.crazybird_daniel1.Graphics.Tela;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Canos {

    private List<Cano> canos = new ArrayList<>();

    private static final int QUANTIDADE_DE_CANOS = 5;
    private static final int DISTANCIA_ENTRE_CANOS = 330;

    private Context context;

    private Tela tela;
    private Pontuacao pontuacao;
    private Som som;

    public Canos (Tela tela, Pontuacao pontuacao, Context context, Som som) {

        this.tela = tela;
        this.pontuacao = pontuacao;
        this.context = context;
        this.som = som;

        int posicaoInicial = 800;

        for (int i = 0; i < QUANTIDADE_DE_CANOS; i++ ) {
            canos.add(new Cano(tela, posicaoInicial, pontuacao, context));
            posicaoInicial += DISTANCIA_ENTRE_CANOS;
        }
    }

    public void desenhaNo(Canvas canvas) {
        for (Cano cano : canos) {
            cano.desenhaNo(canvas);
        }
    }

    public void move() {

        ListIterator<Cano> iterator = canos.listIterator();

        while (iterator.hasNext()) {

            Cano cano = iterator.next();
            cano.move();

            if (cano.saiuDaTela()) {
                pontuacao.aumenta();
                som.play(Som.PONTOS);
                iterator.remove();
                Cano outroCano = new Cano(tela, getMaximo() + DISTANCIA_ENTRE_CANOS, pontuacao, context);
                iterator.add(outroCano);
            }
        }
    }

    private int getMaximo() {
        int maximo = 0;
        for (Cano cano : canos) {
            maximo = Math.max(cano.getPosition(), maximo);

        }
        return maximo;
    }

    public boolean temColisaoCom(Passaro passaro) {
        for (Cano cano : canos) {
            if (cano.temColisaoHorizontalCom(passaro) && cano.temColisaoVerticalCom(passaro)) {
                return true;
            }
        }
        return false;
    }
}
