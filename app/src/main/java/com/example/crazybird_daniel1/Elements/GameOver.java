package com.example.crazybird_daniel1.Elements;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.crazybird_daniel1.Graphics.Cores;
import com.example.crazybird_daniel1.Graphics.Tela;

public class GameOver {

    private static final Paint VERMELHO = Cores.getCorDoGameOver();

    private Tela tela;

    public GameOver(Tela tela) {
        this.tela = tela;
    }

    public void desenhaNo(Canvas canvas) {
        String gameOver = "Game Over";
        canvas.drawText(gameOver,centralizaTexto(gameOver),tela.getAltura() / 2, VERMELHO);
    }

    private int centralizaTexto(String gameOver) {
        Rect limiteDoTexto = new Rect();
        VERMELHO.getTextBounds(gameOver, 0, gameOver.length(), limiteDoTexto);
        int centroHorizontal = tela.getLargura()/2 - (limiteDoTexto.right - limiteDoTexto.left)/2;
        return centroHorizontal;
    }

}
